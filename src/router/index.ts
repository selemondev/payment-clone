import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import Home from "../views/Home.vue";
import Dashboard from "../views/Dashboard.vue";
import UserDashboard from "../views/userDashboard.vue"
const routes: Array<RouteRecordRaw> = [
    {
        path: "/",
        name: "Home",
        component: Home
    },
    {
        path: "/auth/register",
        name: "Register",
        component: () => import("../views/Register.vue")
    },
    {
        path: "/auth/login",
        name: "Login",
        component: () => import("../views/Login.vue")
    },
    {
        path: "/admin/dashboard",
        component: Dashboard,
        children: [
            {
                path: "merchant",
                name: "merchant",
                component: () => import("../views/dashboard/index.vue"),
                meta: {
                    sidebar: true
                },
            },

            {
                path: "admin-order",
                name: "admin-order",
                meta: { sidebar: true },
                component: () => import("../views/dashboard/Order.vue")
            },

            
            {
                path: "admin-transactions",
                name: "admin-transactions",
                meta: { sidebar: true },
                component: () => import("../views/dashboard/Transactions.vue")
            },

            {
                path: "admin-fraud",
                name: "admin-fraud",
                meta: { sidebar: true },
                component: () => import("../views/dashboard/Fraud.vue")
            },

            {
                path: "admin-refunds",
                name: "admin-refunds",
                meta: { sidebar: true },
                component: () => import("../views/dashboard/Refund.vue")
            },

            {
                path: "admin-settings",
                name: "admin-settings",
                meta: { sidebar: true },
                component: () => import("../views/dashboard/Settings.vue")
            }
        ]
    },

    {
        path: "/user/dashboard",
        component: UserDashboard,
        children: [
            {
                path: "profile",
                name: "profile",
                component: () => import("../views/userDashboard/index.vue"),
                meta: {
                    sidebar: true
                },
            },

            {
                path: "order",
                name: "order",
                meta: { sidebar: true },
                component: () => import("../views/userDashboard/Order.vue")
            },

            
            {
                path: "transactions",
                name: "transactions",
                meta: { sidebar: true },
                component: () => import("../views/userDashboard/Transactions.vue")
            },

            {
                path: "fraud",
                name: "fraud",
                meta: { sidebar: true },
                component: () => import("../views/userDashboard/Fraud.vue")
            },

            {
                path: "refunds",
                name: "refunds",
                meta: { sidebar: true },
                component: () => import("../views/userDashboard/Refund.vue")
            },

            {
                path: "settings",
                name: "settings",
                meta: { sidebar: true },
                component: () => import("../views/userDashboard/Settings.vue")
            }
        ]
    },

    {
        path: "/user/onboarding",
        name: "user-onboarding",
        component: () => import("../views/onBoarding/HomeView.vue")
    }
];
const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes
});

export default router;