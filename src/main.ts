import { createApp } from 'vue';
import { createPinia } from 'pinia';
import "./assets/css/tailwind.css";
import { Tab, TabGroup, TabsContent, TabsWrapper } from "./components/VueTailwindOutlineTabs/index";
import Vue3EasyDataTable from 'vue3-easy-data-table';
import 'vue3-easy-data-table/dist/style.css';
import { PrismEditor } from 'vue-prism-editor';
import 'vue-prism-editor/dist/prismeditor.min.css';
import Vue3FormWizard, { FormWizard, TabContent } from 'vue3-form-wizard'
import 'vue3-form-wizard/dist/style.css';
import axiosInterceptor from "./plugin/fetch";
import router from "./router";
import App from './App.vue';
const app = createApp(App);
const pinia = createPinia();
app.use(pinia);
app.use(Vue3FormWizard);
app.use(axiosInterceptor);
app.component('FormWizard', FormWizard);
app.component('TabContent', TabContent)
app.component('EasyDataTable', Vue3EasyDataTable);
app.component('Tab', Tab);
app.component('TabsContent', TabsContent);
app.component('TabsWrapper', TabsWrapper);
app.component('TabGroup', TabGroup);
app.component('PrismEditor', PrismEditor);
app.use(router);
app.mount('#app');
