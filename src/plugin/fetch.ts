import type { AxiosError, AxiosResponse } from "axios";
import { axiosPrivate } from "../api/axios";
import type { App } from "vue";
import { useRouter } from "vue-router";
const router = useRouter();
export default {
    install: (app: App):void => {
        app.config.globalProperties.$http = axiosPrivate;
        const $http = app.config.globalProperties.$http;
        $http.interceptors.request.use(
            function (config: any ) {
                return config;
            },
            function (error: AxiosError) {
                if (error.isAxiosError && !error.response) {
                    // Handle network error
                    console.log(error.message)
                }
                return Promise.reject(error);
            }
        );

        $http.interceptors.response.use(
            function (response: AxiosResponse) {
                return response;
            },
            function (error: AxiosError) {
                if (error.response && error.response.status === 401) {
                    // Handle unauthorized error
                    router.push("/")
                }

                if (error.response && error.response.status === 500) {
                    // Handle unauthorized error
                    console.log(error.message)
                }
                return Promise.reject(error);
            }

        );

        app.provide('$fetch', $http)
    }
}