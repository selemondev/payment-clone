export interface TabProps {
    bgColor?: string,
    width?: string,
    height?: string,
    bgActiveColor?: string
}