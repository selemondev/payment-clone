export const lineData = {
    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
    datasets: [
      {
        label: 'Data One',
        backgroundColor: '#001B6A',
        data: [40, 39, 10, 40, 39, 80, 40]
      }
    ]
  }
  
export const lineOptions = {
    responsive: true,
    maintainAspectRatio: false,
    tooltips: {
      intersect: false
    }
  }
  