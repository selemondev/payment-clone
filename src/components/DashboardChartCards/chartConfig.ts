export const data = {
    labels: [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December'
    ],
    datasets: [
      {
        label: 'Data One',
        backgroundColor: '#001B6A',
        data: [40, 20, 12, 39, 10, 40, 39,99, 40, 20, 12, 11]
      }
    ]
  }
  
export const options = {
    responsive: true,
    maintainAspectRatio: false
}
  