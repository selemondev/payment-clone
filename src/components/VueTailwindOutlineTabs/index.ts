import TabGroup from "./TabGroupOutline.vue";
import Tab from "./TabOutline.vue";
import TabsContent from "./TabsContentOutline.vue";
import TabsWrapper from "./TabsWrapperOutline.vue";
export {
    TabGroup,
    Tab,
    TabsContent,
    TabsWrapper
}
