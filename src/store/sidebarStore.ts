import { defineStore } from "pinia";
export const useSidebarStore = defineStore({
    id: "sidebarStore",
    state: () => ({
        sidebarIsOpen: false as boolean,
        userSidebarIsOpen: false as boolean
    }),
    actions: {
        toggleSidebar(rotate: boolean) {
            this.sidebarIsOpen = this.sidebarIsOpen = rotate;
        },

        toggleUserSidebar(rotate: boolean) {
            this.userSidebarIsOpen = this.userSidebarIsOpen = rotate
        }
    }

});