import axios from "axios";
const accessToken = '';
const baseUrl = '';
export default axios.create({
    baseURL: baseUrl
});

export const axiosPrivate = axios.create({
    baseURL: baseUrl,
    headers: {
        'Content-Type': 'application/json',
        'Access-Control-Origin': '*',
        'Authorization': `Bearer ${accessToken}`
    },
    withCredentials: true,
    timeout: 60 * 1000
});